# # Operators
# Assignment Operator
# a=6
# a-=2
# a*=2
# a/=2
# print(int(a))

# Conditional Operators <, >, <=, >=, !=, =
# Logical Operators and, or, not

# a=10
# if a==10 and type(a)==int:
#     print('Condition True')

# print('Normal Block')

ab='10'
check=ab.isdigit()
if ab=='10' and check==True:
    print('Condition True')
else:
    print('Condition False')

print('Normal Block')